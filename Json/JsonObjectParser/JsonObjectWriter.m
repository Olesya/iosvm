//
//  JsonObjectWriter.m
//  BetYou
//
//  Created by Alexander Kryshtalev on 26.09.12.
//  Copyright (c) 2012 Voodoo Mobile. All rights reserved.
//

#import "JsonObjectWriter.h"
#import "PropertyUtils.h"
#import "SBJsonWriter.h"
#import "VMEntity.h"

@implementation JsonObjectWriter

+ (NSString*) jsonFromObject: (id) object withRoot: (NSString*) root
{
	SBJsonWriter* writer = [[SBJsonWriter alloc] init];
	id result = [[NSDictionary alloc]initWithObjectsAndKeys:[self dictionaryFromObject: object], root, nil];
	return 	[writer stringWithObject: result];
}


+ (NSDictionary*) dictionaryFromObject: (id) object withRoot: (NSString*) root
{
	return [[NSDictionary alloc]initWithObjectsAndKeys:[self dictionaryFromObject: object], root, nil];
}

+ (NSDictionary*) dictionaryFromObject: (id) object
{
	NSMutableDictionary* result = [[NSMutableDictionary alloc] init];
	
	NSDictionary* props = [PropertyUtils classAndParentPropsFor:[object class]];
	for (NSString* propertyName in [props allKeys]) {
		id value = [object valueForKey:propertyName];
		
		if (value == nil) {
			continue;
		}
		
		if ([value isKindOfClass:[NSNumber class]] ||
			[value isKindOfClass:[NSString class]]) {
			
			[result setObject:value forKey:propertyName];
		} else
			if ([value isKindOfClass:[NSArray class]]) {
			NSMutableArray* converted = [[NSMutableArray alloc] initWithCapacity:[value count]];
			for (id arrayItem in value) {
                for (id arrayItem in value) {
                    if ([arrayItem isKindOfClass:[VMEntity class]]){
                        NSDictionary* dictItem = [self dictionaryFromObject:arrayItem];
                        [converted addObject:dictItem];
                    } else {
                        [converted addObject:arrayItem];
                    }
                }
			}
			[result setObject:converted forKey:propertyName];			
		} else {
			NSDictionary* childDictionary = [self dictionaryFromObject:value];
			[result setObject:childDictionary forKey:propertyName];
		}
	}
	return result;
}

@end
