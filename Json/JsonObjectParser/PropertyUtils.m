//
//  PropertyUtils.m
//  JSONObject
//
//  Created by Alexander Kryshtalev on 24.09.12.
//  Copyright (c) 2012 Voodoo Mobile. All rights reserved.
//


// PropertyUtil.m
#import "PropertyUtils.h"
#import "objc/runtime.h"

@implementation PropertyUtils

static const char *getPropertyType(objc_property_t property)
{
    const char *attributes = property_getAttributes(property);
    char buffer[1 + strlen(attributes)];
    strcpy(buffer, attributes);
    char *state = buffer, *attribute = NULL;
    
    while ((attribute = strsep(&state, ",")) != NULL) {
        
        if (attribute[0] == 'T' && attribute[1] != '@') {
            // it's a C primitive type:
            /*
			 if you want a list of what will be returned for these primitives, search online for
			 "objective-c" "Property Attribute Description Examples"
			 apple docs list plenty of examples of what you get for int "i", long "l", unsigned "I", struct, etc.
			 */
            NSMutableData *data = [NSMutableData dataWithBytes:&attribute[1] length:strlen(attribute) - 1];
            [data appendBytes:"\0" length:1];
            return [data bytes];
        }
        else if (attribute[0] == 'T' && attribute[1] == '@' && strlen(attribute) == 2) {
            // it's an ObjC id type:
            return "id";
        }
        else if (attribute[0] == 'T' && attribute[1] == '@') {
            // it's another ObjC object type:
            NSMutableData *data = [NSMutableData dataWithBytes:&attribute[3] length:strlen(attribute) - 4];
            [data appendBytes:"\0" length:1];
            return [data bytes];
        }
    }
    return "";
}


+ (NSDictionary *)classAndParentPropsFor:(Class)klass
{
	NSMutableDictionary *all = [[NSMutableDictionary alloc] init];
	Class current = klass;
	do {
		NSDictionary *result = [PropertyUtils classPropsFor: current];
		[all addEntriesFromDictionary:result];
		current = [current superclass];
	}
	while ([NSStringFromClass(current) compare:NSStringFromClass([NSObject class])] != NSOrderedSame);
	return all;
}

+ (NSDictionary *)classPropsFor:(Class)klass
{
    if (klass == NULL) {
        return nil;
    }
	
    NSMutableDictionary *results = [[NSMutableDictionary alloc] init];
	
    unsigned int outCount, i;
    objc_property_t *properties = class_copyPropertyList(klass, &outCount);
    
    for (i = 0; i < outCount; i++) {
        objc_property_t property = properties[i];
        const char *propName = property_getName(property);
        if(propName) {
            const char *propType = getPropertyType(property);
            NSString *propertyName = [NSString stringWithUTF8String:propName];
            NSString *propertyType = [NSString stringWithUTF8String:propType];
            [results setObject:propertyType forKey:propertyName];
        }
    }
    free(properties);
	
    // returning a copy here to make sure the dictionary is immutable
    return [NSDictionary dictionaryWithDictionary:results];
}

+ (NSArray *)propertyNamesForClass:(Class)klass
{
    NSMutableArray *propertyNames = [[NSMutableArray alloc] init];
    unsigned int propertyCount = 0;
    objc_property_t *properties = class_copyPropertyList(klass, &propertyCount);
    
    for (unsigned int i = 0; i < propertyCount; ++i) {
        objc_property_t property = properties[i];
        const char * name = property_getName(property);
        [propertyNames addObject:[NSString stringWithUTF8String:name]];
    }
    free(properties);
    return [NSArray arrayWithArray:propertyNames];
}

@end
