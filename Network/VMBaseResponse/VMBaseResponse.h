//
//  VMBaseResponse.h
//  RippIn
//
//  Created by Alexander Kryshtalev on 06.12.12.
//  Copyright (c) 2012 Voodoo Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RequestResult.h"

@interface VMBaseResponse : VMEntity

@property (copy) RequestResult *result;

- (BOOL)isSucceeded;

@end
