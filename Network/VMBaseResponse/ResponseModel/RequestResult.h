//
//  Result.h
//  RippIn
//
//  Created by tihonov on 30.10.12.
//  Copyright (c) 2012 Voodoo Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JsonDataProvider.h"
#import "Exception.h"
#import "VMEntity.h"

@interface RequestResult : VMEntity

@property BOOL succeeded;
@property (copy) NSArray *exceptions;
@property (copy) NSNumber *code;

- (NSString *)exceptionsDescription;

@end
