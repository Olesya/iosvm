//
//  Exception.h
//  RippIn
//
//  Created by Alexander Kryshtalev on 09.11.12.
//  Copyright (c) 2012 Voodoo Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Error.h"

@interface Exception : VMEntity

@property (copy) NSString *message;

@end
