//
//  VMRequestExecuteDelegate.h
//  Rippln Chat
//
//  Created by Alexander Kryshtalev on 08.05.13.
//  Copyright (c) 2013 Voodoo Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^RequestDoneBlock)(VMBaseResponse *response);

@protocol VMRequestExecuteDelegate <NSObject>

- (void)executeRequest:(id)requestClass withMethod:(NSString *)method forResponse:(id)responseClass completion:(RequestDoneBlock)completionBlock;
- (void)executeRequestWithBody:(VMBaseRequest *)post forResponse:(id)responseClass completion:(RequestDoneBlock)completionBlock;


@end
