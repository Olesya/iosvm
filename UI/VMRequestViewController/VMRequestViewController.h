//
//  VMRequestViewController.h
//  RippIn
//
//  Created by Alexander Kryshtalev on 06.12.12.
//  Copyright (c) 2012 Voodoo Mobile. All rights reserved.
//

#import "VMBaseViewController.h"

#import "ASIHTTPRequest.h"
#import "SBJsonParser.h"
#import "SBJsonWriter.h"
#import "JsonObjectReader.h"
#import "JsonObjectWriter.h"
#import "VMBaseRequest.h"
#import "VMBaseResponse.h"
#import "VMRequestExecuteDelegate.h"

@interface VMRequestViewController : VMBaseViewController <VMRequestExecuteDelegate>
{
	NSMutableArray *requests;
}

- (void)cancelRequests;
- (ASIHTTPRequest *)createRequestForJsonObject:(VMBaseRequest *)jsonBody;
- (id)createResponseForRequest:(ASIHTTPRequest *)request forClass:(id)theClass;

- (void)executeRequest:(id)requestClass withMethod:(NSString *)method forResponse:(id)responseClass completion:(RequestDoneBlock)completionBlock;
- (void)executeRequestWithBody:(VMBaseRequest *)post forResponse:(id)responseClass completion:(RequestDoneBlock)completionBlock;

@end
