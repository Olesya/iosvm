//
//  BaseValidator.m
//  BetYou
//
//  Created by Alexander Kryshtalev on 19.11.12.
//  Copyright (c) 2012 Voodoo Mobile. All rights reserved.
//

#import "VMBaseValidator.h"

@implementation VMBaseValidator

@synthesize validatingInputs, ownerView;

- (BOOL)validateInput: (id)input;
{
	return TRUE;
}

- (BOOL)validate;
{
	BOOL result = TRUE;
	
	if (self.delegate) {
		[self.delegate willValidate];
	}
	
	int counter = 0;
	for (id input in validatingInputs) {
		int inputResult = [self validateInput:input];
		if (!inputResult) {
			counter++;
		}
		result &= inputResult;
	}
	
	return result;
}

@end
