//
//  EmailValidator.h
//  BetYou
//
//  Created by Alexander Kryshtalev on 19.11.12.
//  Copyright (c) 2012 Voodoo Mobile. All rights reserved.
//

#import "VMBaseValidator.h"

@interface VMEmailValidator : VMBaseValidator

+ (BOOL)isValidEmail:(NSString *)email;

@end
