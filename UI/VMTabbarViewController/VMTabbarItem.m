//
//  TabbarItem.m
//  RippIn
//
//  Created by Alexander Kryshtalev on 19.09.12.
//  Copyright (c) 2012 Voodoo Mobile. All rights reserved.
//

#import "VMTabbarItem.h"
#import "VMTabbarViewController.h"

@implementation VMTabbarItem

@synthesize normalImage, selectedImage;
@synthesize button;

- (id) initForTabbarController: (VMTabbarViewController*) tabbarController
			   controllerClass: (id) _controllerClass
				   normalImage: (UIImage*) _normalImage
				 selectedImage: (UIImage*) _selectedImage

{
	self.normalImage = currentImage = _normalImage;
	self.selectedImage = _selectedImage;
	controllerClass = _controllerClass;
	
	return self;
}

- (id) initForTabbarController: (VMTabbarViewController*) tabbarController
			   controllerClass: (id) _controllerClass
				   normalImage: (UIImage*) _normalImage
				 selectedImage: (UIImage*) _selectedImage
                         title: (NSString*) title
              normalTitleColor: (UIColor*) normalColor
            selectedTitleColor: (UIColor*) selectedColor;
{
    self = [self initForTabbarController:tabbarController controllerClass:_controllerClass normalImage:_normalImage selectedImage:_selectedImage];
    self.title = title;
    self.normalTitleColor = normalColor;
    self.selectedTitleColor = selectedColor;
    
    return self;
}

- (void) setSelected: (bool) selected
{
	currentImage = selected ? self.selectedImage : self.normalImage;

    
    self.label.textColor = selected ? self.selectedTitleColor : self.normalTitleColor;
    
    [self.label setNeedsDisplay];
    
    [self.button setImage:currentImage forState:UIControlStateNormal];
	
	[button setNeedsDisplay];
}

- (UINavigationController*) getController
{
	if (!controller) {
		navController = [[UINavigationController alloc] init];
		
		controller = [[controllerClass alloc] initWithNibName:
					  NSStringFromClass(controllerClass) bundle:nil];
		
		[navController pushViewController:controller animated:YES];
	}
	return navController;
}
@end
