//
//  VMBaseViewController.m
//  RippIn
//
//  Created by Alexander Kryshtalev on 06.12.12.
//  Copyright (c) 2012 Voodoo Mobile. All rights reserved.
//

#import "VMBaseViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "GeometryRect.h"

@implementation VMBaseViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)viewDidAppear:(BOOL)animated
{
    [self registerForKeyboardNotifications];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self unregisterForKeyboardNotifications];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)replaceView:(UIView*)source withAnother:(UIView*)view
{
	CGSize size = source.frame.size;
	view.frame = CGRectMake(0, 0, size.width, size.height);
	
	[source addSubview:view];
}

- (UIViewController *)replaceView: (UIView*)source withController:(id)theClass
{
	UIViewController* controller = [(UIViewController*)[theClass alloc] initWithNibName: NSStringFromClass(theClass) bundle:nil];
	[self replaceView: source withAnother:controller.view];
	return controller;
}


- (UIViewController *)replaceView: (UIView*)source withController:(id)theClass type:(NSString *)animationType subtype:(NSString *)animationSubtype
{
    UIViewController* controller = [(UIViewController*)[theClass alloc] initWithNibName: NSStringFromClass(theClass) bundle:nil];
   
    NSArray *subViews = source.subviews;    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3f;
    transition.type = animationType;
    transition.subtype = animationSubtype;
    [source addSubview:controller.view];
    [source.layer addAnimation:transition forKey:nil];   
    [[subViews objectAtIndex:0] removeFromSuperview];
    return controller;
}

- (UIViewController *)addViewController:(id)theClass withAnimationType:(NSString *)animationType andSubType:(NSString *)animationSubtype
{
    UIViewController* controller = [(UIViewController*)[theClass alloc] initWithNibName: NSStringFromClass(theClass) bundle:nil];
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3f;
    transition.type = animationType;
    transition.subtype = animationSubtype;
    [self.view addSubview:controller.view];
    [self.view.layer addAnimation:transition forKey:nil];
    return controller;
}

- (void)hideViewController:(UIViewController *)controller type:(NSString *)animationType subtype:(NSString *)animationSubtype
{
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3f;
    transition.type = animationType;
    transition.subtype = animationSubtype;
    [controller.view removeFromSuperview];
    [self.view.layer addAnimation:transition forKey:nil];
}

- (void)notImplementedYet
{
	UIAlertView* view = [[UIAlertView alloc] initWithTitle:@"" message:@"This feature or screen is not implemented yet" delegate:self cancelButtonTitle:@"Close" otherButtonTitles: nil];
	[view show];
}

- (void)showError: (NSString *)error
{
	[[[UIAlertView alloc] initWithTitle:@"Error" message:error delegate:nil cancelButtonTitle:@"Close" otherButtonTitles: nil] show];
}

- (void)showInfo: (NSString *)error;
{
	[[[UIAlertView alloc] initWithTitle:@"Info" message:error delegate:nil cancelButtonTitle:@"Close" otherButtonTitles: nil] show];
}

- (void) customNavbarImage: (NSString*) imageName {
	UIImage *image = [UIImage imageNamed: imageName];
	UIImageView *imageView = [[UIImageView alloc] initWithImage: image];
	
	self.navigationItem.titleView = imageView;
}


- (UIBarButtonItem *)customUIBarButtonWithImage:(NSString*)imageName forAction: (SEL)action
{
	UIButton *button = [UIButton buttonWithType: UIButtonTypeCustom];
	UIImage* image = [UIImage imageNamed: imageName];
	[button setImage:image forState:UIControlStateNormal];
	[button addTarget:self action:action forControlEvents: UIControlEventTouchUpInside];
	button.frame = CGRectMake(0.0, 0.0, image.size.width, image.size.height);
	
	UIBarButtonItem *customItem = [[UIBarButtonItem alloc] initWithCustomView: button];
	return customItem;
}

- (UIButton *)customNavBarTitleButtonWithImage:(NSString *)imageName selectedImage:(NSString *)selectedImageName forAction:(SEL)action
{
    UIImage *titleImage = [UIImage imageNamed:imageName];
    UIImage *titleSelectedImage = [UIImage imageNamed:selectedImageName];
    UIButton *titleButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, titleImage.size.width, titleImage.size.height)];
    [titleButton setImage:titleImage forState:UIControlStateNormal];
    [titleButton setImage:titleSelectedImage forState:UIControlStateSelected];
    [titleButton addTarget:self action:action forControlEvents:UIControlEventTouchUpInside];
    return titleButton;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (void)customNavBarTitle: (NSString*) title withFont: (UIFont *)font
{
	title = [title uppercaseString];
	CGSize size = [title sizeWithFont:font];
	UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, size.width, size.height)];
	label.backgroundColor = [UIColor clearColor];
	label.font = font;
	label.textAlignment = NSTextAlignmentCenter;
	label.textColor = [UIColor whiteColor];
	label.text = title;
	self.navigationItem.titleView = label;
}

- (void)showWaitingScreenWithText:(NSString *)text
{
	[self showWaitingScreenOnView:[[[UIApplication sharedApplication] delegate] window].rootViewController.view withText:text];
}

- (void)showWaitingScreenOnView:(UIView *)view withText:(NSString *)text
{
    if (waitingViewController == nil){
        waitingViewController = [[VMWaitingViewController alloc] initWithNibName:@"VMWaitingViewController" bundle:nil];
    }
    
    if (view){
        CGRect frame;
        if ([view isKindOfClass:[UIScrollView class]]){
            UIScrollView *scrollView = (UIScrollView *)view;
            scrollView.bounces = NO;
            scrollView.scrollEnabled = NO;
            frame = scrollView.bounds;
        } else {
            frame = view.frame;
        }
        waitingViewController.view.frame = [GeometryRect moveRectToZero:frame];
        [view addSubview:waitingViewController.view];
    } else {
        CGRect screenRect = [[UIScreen mainScreen] bounds];
        waitingViewController.view.frame = CGRectMake(0, 0, screenRect.size.width, screenRect.size.height);
        [[UIApplication sharedApplication].keyWindow.rootViewController.view addSubview:waitingViewController.view];
    }
    waitingViewController.text = text;
}


- (void)hideWaitingScreen
{
    UIView *superview = waitingViewController.view.superview;
    if ([superview isKindOfClass:[UIScrollView class]]){
        UIScrollView *scrollView = (UIScrollView *)superview;
        scrollView.bounces = YES;
        scrollView.scrollEnabled = YES;
    }
    [waitingViewController.view removeFromSuperview];
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if ([text isEqualToString:@"\n"])
    {
        [textView resignFirstResponder];
    }
    return YES;
}

- (UIViewController *)navigateTo: (id)controllerClass
{
	UIViewController *controller = [[controllerClass alloc] initWithNibName:NSStringFromClass(controllerClass) bundle:nil];
	[self.navigationController pushViewController:controller animated:YES];
	return controller;
}

- (void)navigateBack
{
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)navigateToHome;
{
	[self.navigationController popToRootViewControllerAnimated:YES];
}


- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

- (void)unregisterForKeyboardNotifications
{
	[[NSNotificationCenter defaultCenter] removeObserver:self
													name:UIKeyboardDidShowNotification
												  object:nil];
	
    [[NSNotificationCenter defaultCenter] removeObserver:self
													name:UIKeyboardWillHideNotification
												  object:nil];
}

- (void)keyboardWasShown:(NSNotification*)aNotification
{
    if ([self.view isKindOfClass:[UIScrollView class]]){
        UIScrollView *scrollView = (UIScrollView *)self.view;
        NSDictionary* info = [aNotification userInfo];
        CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
        
        UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
        scrollView.contentInset = contentInsets;
        scrollView.scrollIndicatorInsets = contentInsets;
        
        CGRect aRect = self.view.frame;
        aRect.size.height -= kbSize.height;
        if (!CGRectContainsPoint(aRect, activeField.frame.origin) ) {
            CGPoint scrollPoint = CGPointMake(0.0, activeField.frame.origin.y-kbSize.height);
            [scrollView setContentOffset:scrollPoint animated:YES];
        }
    }
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    if ([self.view isKindOfClass:[UIScrollView class]]){
        UIScrollView *scrollView = (UIScrollView *)self.view;
        UIEdgeInsets contentInsets = UIEdgeInsetsZero;
        scrollView.contentInset = contentInsets;
        scrollView.scrollIndicatorInsets = contentInsets;
    }
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    activeField = textField;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    activeField = nil;
}

- (void)scrollToView:(UIView *)view withNotification:(NSNotification *)aNotification
{
    [self scrollToPoint:CGPointMake(0, view.frame.origin.y + view.frame.size.height) withNotification:aNotification];
}

- (void)scrollToTop
{
    UIScrollView *scrollView = (UIScrollView *)self.view;
    [scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
}

- (void)scrollToPoint:(CGPoint)point withNotification:(NSNotification *)aNotification
{
    if ([self.view isKindOfClass:[UIScrollView class]]) {
		float reduce = 0;
		if (aNotification) {
			NSDictionary* info = [aNotification userInfo];
			CGSize keyboardSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
			reduce = keyboardSize.height;
		}
        
        CGFloat navbarHeight = self.navigationController.navigationBarHidden ? 0 : self.navigationController.navigationBar.frame.size.height;
        
        UIScrollView *scrollView = (UIScrollView *)self.view;
        CGRect screenRect = [[UIScreen mainScreen] bounds];
        CGFloat offset = point.y + [UIApplication sharedApplication].statusBarFrame.size.height - (screenRect.size.height - reduce) + navbarHeight;
        [scrollView setContentOffset:CGPointMake(0, offset) animated:YES];
    }
}

@end
