//
//  GeometryRect.m
//  BetYou
//
//  Created by Olesya Kondrashkina on 12/20/12.
//  Copyright (c) 2012 Voodoo Mobile. All rights reserved.
//

#import "GeometryRect.h"

@implementation GeometryRect

+ (CGRect)moveRect: (CGRect)rect toPoint: (CGPoint)point;
{
    CGRect newRect = CGRectMake(point.x, point.y, rect.size.width, rect.size.height);
    return newRect;
}

+ (CGRect)moveRect: (CGRect)rect bySize: (CGSize)size;
{
	CGRect newRect = CGRectMake(rect.origin.x + size.width, rect.origin.y + size.height, rect.size.width, rect.size.height);
    return newRect;
}

+ (CGRect)moveRectToZero: (CGRect)rect;
{
    CGRect newRect = CGRectMake(0, 0, rect.size.width, rect.size.height);
    return newRect;
}

+ (CGRect)centerRect: (CGRect)rect intoRect: (CGRect)outerRect;
{
    CGPoint newCenter = CGPointMake(outerRect.origin.x + outerRect.size.width / 2, outerRect.origin.y + outerRect.size.height / 2);
    CGPoint newOrigin = CGPointMake(newCenter.x - rect.size.width / 2, newCenter.y - rect.size.height / 2);
    CGRect newRect = CGRectMake(newOrigin.x, newOrigin.y, rect.size.width, rect.size.height);
    return newRect;
}

+ (CGRect)centerRect: (CGRect)rect atPoint: (CGPoint)point;
{
	CGSize size = rect.size;
	return CGRectMake(point.x - size.width / 2, point.y - size.height / 2, size.width, size.height);
}

+ (CGRect)resizeRect: (CGRect)rect bySize: (CGSize)size;
{
	return CGRectMake(rect.origin.x, rect.origin.y, rect.size.width + size.width, rect.size.height + size.height);
}

+ (CGRect)resizeRect: (CGRect)rect setSize: (CGSize)size;
{
	return CGRectMake(rect.origin.x, rect.origin.y, size.width, size.height);
}

@end
