//
//  ImageHelper.h
//  GSPrice
//
//  Created by Dmitry Tihonov on 02.04.13.
//  Copyright (c) 2013 Voodoo Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ImageHelper : NSObject

+ (UIImage*)imageFromView:(UIView *)view;
+ (void) applyMaskNamed:(NSString*)mask toView:(UIView*)view;

@end
