//
//  ImageHelper.m
//  GSPrice
//
//  Created by Dmitry Tihonov on 02.04.13.
//  Copyright (c) 2013 Voodoo Mobile. All rights reserved.
//

#import "ImageHelper.h"
#import <QuartzCore/QuartzCore.h>

@implementation ImageHelper

+ (UIImage*)imageFromView:(UIView *)view
{
    // Create a graphics context with the target size
    // On iOS 4 and later, use UIGraphicsBeginImageContextWithOptions to take the scale into consideration
    // On iOS prior to 4, fall back to use UIGraphicsBeginImageContext
    CGSize imageSize = [view bounds].size;
    if (NULL != UIGraphicsBeginImageContextWithOptions)
        UIGraphicsBeginImageContextWithOptions(imageSize, NO, 0);
    else
        UIGraphicsBeginImageContext(imageSize);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // -renderInContext: renders in the coordinate space of the layer,
    // so we must first apply the layer's geometry to the graphics context
    CGContextSaveGState(context);
    // Center the context around the view's anchor point
    CGContextTranslateCTM(context, [view center].x, [view center].y);
    // Apply the view's transform about the anchor point
    CGContextConcatCTM(context, [view transform]);
    // Offset by the portion of the bounds left of and above the anchor point
    CGContextTranslateCTM(context,
                          -[view bounds].size.width * [[view layer] anchorPoint].x,
                          -[view bounds].size.height * [[view layer] anchorPoint].y);
    
    // Render the layer hierarchy to the current context
    [[view layer] renderInContext:context];
    
    // Restore the context
    CGContextRestoreGState(context);
    
    // Retrieve the screenshot image
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return image;
}

+ (void) applyMaskNamed:(NSString*)mask toView:(UIView*)view;
{
	// Set images
	CALayer *layerMask = [CALayer layer];
	UIImage* image = [UIImage imageNamed:mask];
	layerMask.contents = (id)[image CGImage];
	
	layerMask.frame = CGRectMake(0, 0, view.frame.size.width, view.frame.size.height);
	
	view.layer.masksToBounds = YES;
	view.layer.mask = layerMask;
}

@end
