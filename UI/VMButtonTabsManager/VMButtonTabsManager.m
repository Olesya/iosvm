//
//  ButtonTabsManager.m
//  RippIn
//
//  Created by Alexander Kryshtalev on 06.10.12.
//  Copyright (c) 2012 Voodoo Mobile. All rights reserved.
//

#import "VMButtonTabsManager.h"


@implementation VMButtonTabsManager

@synthesize delegate, controllers = _controllers;

- (id) init
{
	self = [super init];
	buttons = [[NSMutableArray alloc] init];
	self.controllers = [[NSMutableArray alloc] init];
	_selectedIndex = -1;
	
	return self;
}

- (id) initWithButtons: (NSArray *)_buttons forControllers: (NSArray *)_theControllers andPlaceholder: (UIView *)_placeholder
{
	self = [self init];
	
	buttons = [[NSMutableArray alloc] initWithArray:_buttons];
	self.controllers = [[NSMutableArray alloc] initWithArray:_theControllers];
	[self addActions ];
	
	placeholderView = _placeholder;
	self.selectedIndex = 0;
	return self;
}

- (void)setButtons: (NSArray* )_buttons forControllerClasses:(NSArray *)_classes
{
	assert(_buttons.count == _classes.count);
	buttons = [[NSMutableArray alloc] initWithArray:_buttons];

	self.controllers = [[NSMutableArray alloc] initWithCapacity:_classes.count];
	for (id class in _classes) {
		UIViewController *controller =[[class alloc] initWithNibName:NSStringFromClass(class) bundle:nil];
		[self.controllers addObject:controller];
	}
	
	[self addActions ];
	self.selectedIndex = 0;
}

- (id) initWithButtons: (NSArray *)_buttons forControllerClasses: (NSArray *)_classes andPlaceholder: (UIView *)_placeholder
{
	NSMutableArray *created = [[NSMutableArray alloc] initWithCapacity:_classes.count];
	
	for (id class in _classes) {
		UIViewController *controller =[[class alloc] initWithNibName:NSStringFromClass(class) bundle:nil];
		[created addObject:controller];
	}

	self = [self initWithButtons:_buttons forControllers:created andPlaceholder:_placeholder];
	return self;
}

- (id) initWithPlaceholder: (UIView *)_placeholder
{
	id result = [self init];
	
	placeholderView = _placeholder;
	return result;
}

- (void) addController: (UIViewController *)controller forButton: (UIButton *)button
{
	[self.controllers addObject:controller];
	[buttons addObject:button];
	
	[button addTarget:self action:@selector(onButton:) forControlEvents:UIControlEventTouchUpInside];

	if (self.selectedIndex == -1)
		self.selectedIndex = 0;
}

- (void) addActions
{
	for (UIButton *button in buttons) {
		[button addTarget:self action:@selector(onButton:) forControlEvents:UIControlEventTouchUpInside];
	}
}

- (void)onButton: (id)sender
{
	self.selectedIndex = [buttons indexOfObject:sender];
}

- (UIViewController *) createController: (id)controllerClass forButton: (UIButton *)button
{
	NSLog(@"%@", NSStringFromClass(controllerClass));
	UIViewController * controller = [[controllerClass alloc] initWithNibName:NSStringFromClass(controllerClass) bundle:nil];
	
	CGSize size = placeholderView.frame.size;
	CGRect frame = CGRectMake(0, 0, size.width, size.height);
	controller.view.frame = frame;
	
	[self addController: controller forButton:button];
	return controller;
}

- (UIButton *)buttonForIndex: (NSUInteger)index
{
	return [buttons objectAtIndex:index];
}

- (NSUInteger) count;
{
	return buttons.count;
}

- (NSUInteger) getSelectedIndex
{
	return _selectedIndex;
}

- (void)setSelectedIndex:(NSUInteger)index
{
	if (_selectedIndex != index) {
		if (_selectedIndex != -1) {
			UIButton *button = [buttons objectAtIndex:_selectedIndex];
			UIViewController *controller = [self.controllers objectAtIndex:_selectedIndex];
			
			button.selected = false;
			[controller.view removeFromSuperview];
		}
		
		_selectedIndex = index;
		
		UIButton *button = [buttons objectAtIndex:index];
		UIViewController *controller = [self.controllers objectAtIndex:index];
		
		button.selected = true;
        
        controller.view.frame = CGRectMake(0, 0, placeholderView.frame.size.width, placeholderView.frame.size.height);
        
		[placeholderView addSubview:controller.view];
		
		if (delegate != nil)
			[delegate didChangeSelection:index];
	}
}

@end
