//
//  ButtonTabsManager.h
//  RippIn
//
//  Created by Alexander Kryshtalev on 06.10.12.
//  Copyright (c) 2012 Voodoo Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VMButtonTabsManagerDelegate.h"

@interface VMButtonTabsManager : NSObject
{
	UIView *placeholderView;
	NSMutableArray *buttons;
}

- (UIButton *)buttonForIndex: (NSUInteger)index;

@property id <VMButtonTabsManagerDelegate> delegate;
@property (nonatomic, readonly) NSUInteger count;
@property (nonatomic) NSUInteger selectedIndex;
@property NSMutableArray *controllers;

- (void)setButtons: (NSArray* )buttons forControllerClasses:(NSArray *)classes;
- (id) initWithButtons: (NSArray *)buttons forControllers: (NSArray *)controllers andPlaceholder: (UIView *)placeholder;
- (id) initWithButtons: (NSArray *)buttons forControllerClasses: (NSArray *)classes andPlaceholder: (UIView *)placeholder;

- (id) initWithPlaceholder: (UIView *)placeholder;
- (void)addController: (UIViewController *)controller forButton: (UIButton *)button;
- (UIViewController *)createController: (id)controllerClass forButton: (UIButton *)button;

@end
