//
//  CanvasViewDelegate.h
//  NDA
//
//  Created by Dmitry Tihonov on 30.01.13.
//  Copyright (c) 2013 Voodoo Mobile. All rights reserved.
//

#ifndef NDA_CanvasViewDelegate_h
#define NDA_CanvasViewDelegate_h

@protocol CanvasViewDelegate <NSObject>

- (void)beginDrawing;
- (void)canvasDidClear;

@end

#endif
