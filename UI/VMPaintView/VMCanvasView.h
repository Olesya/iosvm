//
//  UICanvasView.h
//  NDA
//
//  Created by Alexander Kryshtalev on 28.01.13.
//  Copyright (c) 2013 Voodoo Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CanvasViewDelegate.h"

@interface VMCanvasView : UIView
{
	CGPoint location;
}

@property (copy) UIColor *color;
@property id <CanvasViewDelegate> delegate;

- (void)clear;
- (UIImage *)getImage;

@end
